package com.controller;

import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Place;
import com.model.image;
import com.service.placeService;

@RestController
@CrossOrigin
public class placeController {

	@Autowired
	private placeService placeservice;

	@PostMapping(value = "addplace")
	public Place addplace(@RequestBody Place place) {
		return placeservice.addplace(place);
	}

	@PutMapping(value = "updateplace/{contact}/{price}/{id}")
	public void updateplaceDetails(@PathVariable("contact") String Contact, @PathVariable("price") float price,
			@PathVariable("id") int id) {
		 placeservice.updateplace(Contact, price, id);
	}

	@PutMapping(value="updatestatus/{id}/{status}")
	public void updatePlaceStatus(@PathVariable("id") int id, @PathVariable("status") String status) {
		 placeservice.reservePlace(id, status);
	}
	@DeleteMapping(value = "deleteplace/{id}")  //udated
	public void deletePlaceById(@PathVariable("id") int id) {
		 placeservice.deletePlaceById(id);
	}
	

	@GetMapping("findallplaces")
	public List<Place> findAllplaces(){
		return placeservice.getallplaces();
	}
	@PutMapping("updatepname/{name}/{pid}")
	public void updateByPlaceName(@PathVariable("name") String name ,@PathVariable("pid") int pid) {
		placeservice.updatePlaceName(name, pid);
	}
	
	@PutMapping("updatepprice/{price}/{pid}")
	public void updatePlaceByPrice(@PathVariable("price") float price ,@PathVariable("pid") int pid) {
		placeservice.updateplaceprice(price, pid);
	}
	
	@PutMapping("updateploc/{name}/{pid}")
	public void updatePlaceLoc(@PathVariable("name") String name ,@PathVariable("pid") int pid) {
		placeservice.updateplacelocation(name, pid);
	}
	@PutMapping("updatepdesc/{name}/{pid}")
	public void updatePlaceDescription(@PathVariable("name") String name ,@PathVariable("pid") int pid) {
		placeservice.updatePlaceDescription(name, pid);
	}
	@PutMapping("updateprate/{rate}/{pid}")
	public void updateplaceRate(@PathVariable("rate") int name ,@PathVariable("pid") int pid) {
		placeservice.updatePlacePrice(name, pid);
	}
	@PutMapping("updateptype/{type}/{pid}")
	public void updatePlaceType(@PathVariable("type") String name ,@PathVariable("pid") int pid) {
		placeservice.updatePlaceType(name, pid);
	}
	@PutMapping("updatepcontact/{contact}/{pid}")
	public void updateplacecontact(@PathVariable("contact") String name ,@PathVariable("pid") int pid) {
		placeservice.updateplacecontact(name, pid);
	}
	@GetMapping("searchplaces/{place}")
	public List<Place> searchplacesByPlacename(@PathVariable("place") String place){
		return placeservice.searchplaceByPlacename(place);
	}
	@GetMapping("searchplacesbyfeature"
			+ "/{place}")
	public List<Place> getPlacesByFeatures(@PathVariable("place") String place){
		return placeservice.getPlaceByFeatures(place);
	}

	
	@GetMapping(value = "top2bottom")
	public List<Place> searchPlacesFromToptoBottom(){
		return placeservice.searchPlacesFromToptoBottom();
	}
	@GetMapping(value = "bottom2top")
	public List<Place> searchPlacesFromBottomtoTop(){
		return placeservice.getplacedbt();
	}
	

}
