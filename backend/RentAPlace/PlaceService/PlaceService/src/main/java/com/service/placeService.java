package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Place;
import com.model.image;
import com.repository.placeRepo;

@Service
public class placeService {
	
	@Autowired
	private placeRepo placeRepository;
	
	
//	method to add the place
	public Place addplace(Place place) {
		return placeRepository.save(place);
	}
	
// method to update place
	public void updateplace(String contact,float price,int id) {
		 placeRepository.updateplace(contact,price,id);
	}
	
//method to reserve the place 
	
	public void reservePlace(int id,String status) {
		 placeRepository.updatestatus(id,status);
	}
// method to get the places based on rating (top-bottom)
	
	public List<Place> searchPlacesFromToptoBottom(){
		return placeRepository.findplacesBasedonrating();
	}
	
//	method to get the places based on rating (low-top)
	public List<Place> getplacedbt(){
		return placeRepository.getplacebt();
	}
	
//	method to delete the place
	
	public void deletePlaceById(int id) {
		Place p = placeRepository.getById(id);
		placeRepository.delete(p);
	}
	
//	method to get all the places 
	public List<Place> getallplaces(){
		return placeRepository.findAll();
	}
	
	public void updatePlaceName(String name ,int pid) {
		placeRepository.updateplacename(name, pid);
	}
	public void updateplaceprice(float price ,int pid) {
		placeRepository.updateplaceprice(price, pid);
	}
	
	public void updateplacelocation(String name ,int pid) {
		placeRepository.updateplacelocation(name, pid);
	}
	public void updateplacecontact(String name ,int pid) {
		placeRepository.updateplacecontact(name, pid);
	}
	public void updatePlaceDescription(String name ,int pid) {
		placeRepository.updateplacedesc(name, pid);
	}
	public void updatePlacePrice(int rate ,int pid) {
		placeRepository.updateplacerating(rate, pid);
	}
	public void updatePlaceType(String name ,int pid) {
		placeRepository.updateplacetype(name, pid);
	}
	
//	search place by location
	public List<Place> searchplaceByPlacename(String place){
		return placeRepository.searchplace(place);
	}
	
	public List<Place> getPlaceByFeatures(String place){
		return placeRepository.searchplacebyfeatures(place);
	}

}
